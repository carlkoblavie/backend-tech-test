"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const app = express();
const nextPrimeNumberController = require("./controllers/nearestPrimeNumber");
app.get('/api/nearest_prime/:num', nextPrimeNumberController.fetch);
app.get('*', (req, res) => {
    res.status(404).send('You entered an invalid route. Please check and try again.');
});
app.set("port", process.env.PORT || 7000);
exports.default = app;
//# sourceMappingURL=app.js.map
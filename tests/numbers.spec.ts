import app from '../src/app';
import * as chai from 'chai';
import 'mocha';

const Numbers = require('../src/lib/numbers');
const assert = chai.assert;

describe('isPrime', () => {
  it('should return false when number is not a prime number', () => {
    const response =  Numbers.isPrime(4);
    assert.isFalse(response);
  })

  it('should return true when number is a prime number', () => {
    const response =  Numbers.isPrime(5);
    assert.isTrue(response);
  })
})
import app from '../src/app';
import * as chai from 'chai';
import chaiHttp = require('chai-http');

import 'mocha';

chai.use(chaiHttp);
const expect = chai.expect;

describe('GET /api/nearest_prime/:num', () => {
  it('should return 2 as the nearest prime number when given 0', async () => {
    const num = 0;
    const nearestPrimeNumber = 2;
    const response = await chai.request(app).get('/api/nearest_prime/' + num)
    chai.expect(response.status).to.be.equal(200);
    chai.expect(response.body).to.have.property('nearest_prime_number', nearestPrimeNumber);
  })

  it('should return 2 as the nearest prime number when given a negative number', async () => {
    const num = -1;
    const nearestPrimeNumber = 2;
    const response = await chai.request(app).get('/api/nearest_prime/' + num)
    chai.expect(response.status).to.be.equal(200);
    chai.expect(response.body).to.have.property('nearest_prime_number', nearestPrimeNumber);
  })

  it('should return the nearest prime number when given a positive number', async () => {
    const num = 14;
    const nearestPrimeNumber = 13;
    const response = await chai.request(app).get('/api/nearest_prime/' + num)
    chai.expect(response.status).to.be.equal(200);
    chai.expect(response.body).to.have.property('nearest_prime_number', nearestPrimeNumber);
  })

  it('should return status 400 if route parameter is not a number', async () => {
    const num = 'x';
    const response = await chai.request(app).get('/api/nearest_prime/' + num)
    chai.expect(response.status).to.be.equal(400);
    chai.expect(response.text).to.match(/^[a-zA-Z]+/);
  })

  it('should return status 404 if requested endpoint doesn\'t exist', async () => {
    const response = await chai.request(app).get('/')
    chai.expect(response.status).to.be.equal(404);
    chai.expect(response.text).to.match(/^[a-zA-Z]+/);
  })
})

import express = require("express");
const app = express();

import * as nextPrimeNumberController from './controllers/nearestPrimeNumber';

app.get('/api/nearest_prime/:num', nextPrimeNumberController.fetch);

app.get('*', (req, res) => {
    res.status(404).send('You entered an invalid route. Please check and try again.');
})

app.set("port", process.env.PORT || 7000);

export default app;

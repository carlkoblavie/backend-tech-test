module.exports.isPrime = function (number: number): boolean {
  if (number < 0) return false;
  for(let i = 2; i < number; i++) {
    if(number % i === 0) return false; 
  }
  return number > 1;
}
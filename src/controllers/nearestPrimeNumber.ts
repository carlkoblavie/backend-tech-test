import { Request, Response } from 'express'
const Numbers = require('../lib/numbers');
export let fetch = (req: Request, res: Response) => {
  try {
    const parsedParam = parseInt(req.params.num, 10);
    if (isNaN(parsedParam)) throw new Error('Please provide a number as a parameter');
    let firstIteration: number = req.params.num;
    let secondIteration: number = req.params.num;
    let nearest_prime_number: number;

    if(parsedParam > 0) {
      do {
        firstIteration--
      } while (!Numbers.isPrime(firstIteration) && firstIteration > 0);
    }
    
    do {
      secondIteration++
    } while (!Numbers.isPrime(secondIteration));

    if(parsedParam <= 0 ) {
      nearest_prime_number = secondIteration;
    } else {
      nearest_prime_number = ((parsedParam - firstIteration) < (secondIteration - parsedParam)) ? firstIteration : secondIteration;
    }
    
    res.send({nearest_prime_number: nearest_prime_number});
  } catch (error) {
    res.status(400).send(error.message || 'Something went wrong. Please try again');
  }
}
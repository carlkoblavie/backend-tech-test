# A Million Ads Backend Test

To Run tests: npm run test

To start application: npm run serve

To test in a browser, use the url http://localhost:7000/api/nearest_prime/:num

where :num is the route parameter needed to get the nearest prime number.